﻿#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeexeption.h"
#include "InputException.h"
#include "input2Exception.h"
#include "Pentagon.h"
#include "Hexagon.h" 

using namespace std;

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 0; int height = 0, width = 0, side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pen(nam, col, side);
	Hexagon hex(nam, col, side);

	Shape* ptrcirc = &circ;
	Shape* ptrquad = &quad;
	Shape* ptrrec = &rec;
	Shape* ptrpara = &para;
	Shape* ptrpen = &pen;
	Shape* ptrhex = &hex;



	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e = pentagon, h = hexagon" << std::endl;
		std::cin >> shapetype;
		try
		{
			if (cin.rdbuf()->in_avail() > 1)
			{
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
				throw Input2Exception();
			}
			else
			{
				switch (shapetype) {
				case 'c':
					std::cout << "enter color, name,  rad for circle" << std::endl;
					std::cin >> col >> nam >> rad;
					if (cin.fail())
					{
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						throw InputException();
					}
					circ.setColor(col);
					circ.setName(nam);
					circ.setRad(rad);
					ptrcirc->draw();
					break;
				case 'q':
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					if (cin.fail())
					{
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						throw InputException();
					}
					quad.setName(nam);
					quad.setColor(col);
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
					break;
				case 'r':
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					if (cin.fail())
					{
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						throw InputException();
					}
					rec.setName(nam);
					rec.setColor(col);
					rec.setHeight(height);
					rec.setWidth(width);
					ptrrec->draw();
					break;
				case 'p':
					std::cout << "enter name, color, height, width, 2 angles" << std::endl;
					std::cin >> nam >> col >> height >> width >> ang >> ang2;
					if (cin.fail())
					{
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						throw InputException();
					}
					para.setName(nam);
					para.setColor(col);
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
					ptrpara->draw();
					break;
				case 'e':
					std::cout << "enter name, color, and side of petagon" << std::endl;
					std::cin >> nam >> col >> side;
					if (cin.fail())
					{
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						throw InputException();
					}
					pen.setName(nam);
					pen.setColor(col);
					pen.setSide(side);
					ptrpen->draw();
				case 'h':
					std::cout << "enter name, color, and side of hexagon" << std::endl;
					std::cin >> nam >> col >> side;
					if (cin.fail())
					{
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						throw InputException();
					}
					hex.setName(nam);
					hex.setColor(col);
					hex.setSide(side);
					ptrhex->draw();
				default:
					std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
					break;
				}
				std::cout << "would you like to add more object press any key if not press x" << std::endl;
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
				cin.get(x);

			}
			}
		catch (shapeException &e)
		{
			printf(e.what());	
		}
		catch (InputException &e)
		{
			printf(e.check());	
		}
		catch (Input2Exception &e)
		{
			printf(e.check2());
		}
		
	}



	system("pause");
	return 0;

}