#include "Mathutils.h"
#include <math.h>
#include <iostream>


double Mathutils::calPentagonArea(double a)
{
	double area = 0;
	area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * a * a) / 4;
	return area;
}

double  Mathutils::CalHexagonArea(double a)
{
	double area = 0;
	area = ((3 * sqrt(3) * (a * a)) / 2);
	return area;
}