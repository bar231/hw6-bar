#pragma once
#include "Shape.h"
#include "MathUtils.h"

class Pentagon : public Shape
{
private:
	static double _side;

public:
	Pentagon(std::string, std::string, double);
	void draw();
	double CalArea();
	void setSide(double);


};
