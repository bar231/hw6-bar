#include "Hexagon.h"

double Hexagon::_side = 0;

Hexagon::Hexagon(std::string name, std::string color, double side) : Shape(name, color)
{
	setSide(_side);
}

void Hexagon::setSide(double side)
{
	_side = side;
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "side is " << _side << "Area is: " << CalArea() << std::endl;;
}


double Hexagon::CalArea()
{
	return Mathutils::CalHexagonArea(_side);
}




