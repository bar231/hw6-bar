#include <exception>
#pragma once
class Input2Exception : public std::exception
{
public:
	virtual const char* check2() const
	{
		return "Warning - Dont try to build more than one shape at once";
	}
};
