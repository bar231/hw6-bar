#pragma once
#include "Shape.h"
#include "MathUtils.h"

class Hexagon : public Shape
{
private:
	static double _side;

public:
	Hexagon(std::string, std::string, double);
	void draw();
	double CalArea();
	void setSide(double);


};

