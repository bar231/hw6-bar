#include <iostream>
using namespace std;

void checkParams(int a, int b, int val, bool *error)
{
	if (a == 8200 || b == 8200 || val == 8200)
	{
		*error = true;
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year";
	}
}

int add(int a, int b, bool *error)
{
	checkParams(a, b, a + b, error);
	if (*error)
	{
		return -1;
	}
	return a + b;
}

int  multiply(int a, int b, bool* error)
{
	int sum = 0;
	for (int i = 0; i < b; i++)
	{
		sum = add(sum, a, error);
		if (*error)
		{
			return -1;
		}
		checkParams(a, b, sum, error);
	};
	return sum;
}

int  pow(int a, int b, bool* error)
{
	int exponent = 1;
	for (int i = 0; i < b; i++)
	{
		exponent = multiply(exponent, a, error);
		if (*error)
		{
			return -1;
		}
		checkParams(a, b, exponent, error);

	};
	return exponent;
}

int main(void)
{
	bool error = false;
	std::cout << add(8000, 200, &error) << std::endl;

}